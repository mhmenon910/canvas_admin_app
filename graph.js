if(window.addEventListener) {
window.addEventListener('load', function () {



// variable definitions
var draw_path = [];
var adj_list = [];
var nodes = [];
var weight = [];
var i=1,n1,n2,j,k;
var v1,v2;
var count = 0;
nodes[0] = [0,0];
adj_list[0] = 0;
var POI = [];
var j=0;

// listener calls
canvas.addEventListener("dblclick", handleDoubleClick, false); 
canvas.addEventListener("mouseup", handleMouseUp, false);
canvas.addEventListener("mousedown", handleMouseDown, false);




// functions
    function confirmation_for_access_point()
  {
    alertify.confirm("You want to mark it as a access point", function (e) {
    if (e) {
      dialog_enter_details();
     }
  });
  }



function dialog_enter_details()
  {  
    alertify.prompt("Enter Access Point Name", function (e, str) {  
      
      if (e) {
       if(str != "Default Value"){
       draw_access_point(str);
}
     else
      alertify.alert("Enter Access Point Name");
      } else {
      }
  }, "Default Value");
  }
   


function draw_access_point(str){

    var cell = getpos(e);

    var poi_hash = {};
    ctx.beginPath();
    ctx.arc(startX, startY, 6, 0, Math.PI*2, true); 
    ctx.closePath();
    ctx.fillStyle = "#c82124"; 
    ctx.fill();
    poi_hash[cell.x+","+cell.y] = str;
    POI.push(poi_hash)

    i= i+1;
    var div = document.getElementById('labels_for_poa');
    div.style.display = 'block';
    
    div.innerHTML = div.innerHTML + "<br>"+i+". &nbsp;"+ poi_hash[cell.x+","+cell.x]+"&nbsp;"+"("+cell.x+","+cell.x+")";

  $('#floor_point_floor_coordinates').val(JSON.stringify(POI));
    
  } 


function handleDoubleClick(e)
{ 

        var msg = "";
    	var cell = getpos(e);

    	canvasX = cell.x-10;
    	canvasY = cell.y-10;
    	nodes[i]=[cell.x,cell.y];

		adj_list[i]=[];
    	var g_canvas = document.getElementById("canvas");
    	var context = g_canvas.getContext("2d");
	context.strokeStyle = "black";
    	var cat = new Image();
    	cat.src = "/home/manoj/Videos/canvas_app/images/dot.png";

        if (getpos != null) {
                msg =  cell.x +"," + cell.y;
        }
       

cat.onload = function() 
    	{
    	    context.drawImage(cat,canvasX , canvasY);
            
            context.fillText(i, canvasX+10, canvasY+30);
            i=i+1;
        }
    alertify.prompt("Enter Access Point Name", function (e, str) {  
      
      if (e) {
       if(str != "Default Value"){

      // draw_access_point(str);
    var poi_hash = {};
   poi_hash[cell.x+","+cell.y] = str;
    POI.push(poi_hash)



   var div = document.getElementById('labels_for_poa');
    div.style.display = 'block';
    j = j+1;
    div.innerHTML = div.innerHTML + "<br>"+j+". &nbsp;"+ poi_hash[cell.x+","+cell.y]+"&nbsp;"+"("+cell.x+","+cell.x+")";
 div.innerHTML = div.innerHTML + "<br>";
  $('#floor_point_floor_coordinates').val(JSON.stringify(POI));
}
     else
      alertify.alert("Enter Access Point Name");
      } else {
      }
  }, "Default Value");
}


function getpos(e) 
{
    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement = this;
    var x;
    var y;
    if (e.pageX != undefined && e.pageY != undefined) 
    {
        x = e.pageX;
        y = e.pageY;
    }
    else 
    {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x = x- canvas.offsetLeft;
    y = y- canvas.offsetTop;
    
    return {x:x, y:y}
}


var x1=0,x2=0,y1=0,y2=0;

function handleMouseDown(e)
{


    var c=document.getElementById("canvas");
    var cxt=c.getContext("2d");
    cxt.strokeStyle = "black";
    var cell = getpos(e);

    for(var k=1;k<=nodes.length;k++)
    {
    	if(cell.x>=nodes[k][0]-10 && cell.x<=nodes[k][0]+10 && cell.y>=nodes[k][1]-10 && cell.y<=nodes[k][1]+10)
    	{
    	 	x1 = nodes[k][0];
    		y1 = nodes[k][1];
    		for(j=1;j<=nodes.length;j++)
    		{
    				if(x1==nodes[j][0] && y1==nodes[j][1]){
    					n1=j;
    				}
    		}
    
    	}
    	
   
}
}
    	var g_canvas = document.getElementById("canvas");
    	var context = g_canvas.getContext("2d");
        var background = new Image();
        background.src = "http://s23.postimg.org/jhgzxhj8b/chi_map_level_1_png.png";


        background.onload = function(){
         context.drawImage(background,0,0);   
        }

function handleMouseUp(e)
{
    var cell = getpos(e);

    for(k=1;k<=nodes.length;k++)
    {

    	if(cell.x>=nodes[k][0]-10 && cell.x<=nodes[k][0]+10 && cell.y>=nodes[k][1]-10 && cell.y<=nodes[k][1]+10)
    	{
    	 	x2 = nodes[k][0];
    		y2 = nodes[k][1];
    		var c=document.getElementById("canvas");
    		var cxt=c.getContext("2d");
    		cxt.strokeStyle = "red";
		cxt.moveTo(x1,y1);
    		cxt.lineTo(x2,y2);
		cxt.lineWidth = 3;
    		cxt.stroke(); 
		v1=x2-x1;
		v2=y2-y1;
		wt = Math.sqrt((v1*v1)+(v2*v2));
		wt = parseInt(wt);

 		for(j=1;j<=nodes.length;j++)
    		{
    				if(x2==nodes[j][0] && y2==nodes[j][1]){
    					n2=j;
    					adj_list[n1].push(n2);  					    		
					adj_list[n2].push(n1);
					weight.push(wt,n1,n2);
                console.log(n1,n2,wt);

                var div = document.getElementById('show_distance');
                div.style.display = 'block';
    
                div.innerHTML = div.innerHTML + "<br>"+n1+"  to &nbsp;"+ n2+"&nbsp;"+"------------>"+wt;

div.innerHTML = div.innerHTML + "<br>"+n2+"  to &nbsp;"+ n1+"&nbsp;"+"------------>"+wt;
 div.innerHTML = div.innerHTML + "<br>";

    				}
    		}   		
    	}
    	  	
    

}
}
}, false); }
